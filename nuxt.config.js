module.exports = {
  modules: [
    '@nuxtjs/axios',
    'nuxt-google-maps-module',
    ['nuxt-sass-resources-loader', '@/layouts/variables.scss'],
  ],

  axios: {
    // debug: true,
    // proxyHeaders: false
  },

  maps: {
    key: 'AIzaSyCVi9VH40Wkpcv2nlTZ-g6O8jRb3ZgDHU4',
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Funda',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Funda programming assignment for front-end developers' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#f7a100' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

