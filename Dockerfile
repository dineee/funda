FROM node:10-alpine
WORKDIR /src

ADD yarn.lock /src/
ADD package.json /src/
RUN yarn install

ENV HOST=0.0.0.0
ENV PORT=3000

ADD . /src
RUN yarn build

EXPOSE 3000
CMD ["yarn", "run", "start"]
